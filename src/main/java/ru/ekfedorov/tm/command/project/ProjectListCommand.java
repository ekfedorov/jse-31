package ru.ekfedorov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.service.IProjectService;
import ru.ekfedorov.tm.command.AbstractProjectCommand;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.exception.system.NullComparatorException;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show all project.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-list";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (serviceLocator == null) throw new NullObjectException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final @Nullable String sort = TerminalUtil.nextLine();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @Nullable List<Project> projects;
        if (isEmpty(sort)) projects = projectService.findAll(userId);
        else {
            @NotNull final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            @NotNull final Comparator<Project> comparator = sortType.getComparator();
            projects = projectService.findAll(userId, comparator);
        }
        if (projects.isEmpty()) {
            System.out.println("--- There are no projects ---");
            return;
        }
        int index = 1;
        for (@NotNull final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println();
    }

}
