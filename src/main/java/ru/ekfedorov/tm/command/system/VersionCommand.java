package ru.ekfedorov.tm.command.system;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.command.AbstractCommand;
import ru.ekfedorov.tm.exception.system.NullObjectException;

public final class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandArg() {
        return "-v";
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Display program version.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "version";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (serviceLocator == null) throw new NullObjectException();
        System.out.println("[VERSION]");
        System.out.println(Manifests.read("version"));
    }

}
