package ru.ekfedorov.tm.command.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.service.ICommandService;
import ru.ekfedorov.tm.command.AbstractCommand;
import ru.ekfedorov.tm.exception.system.NullObjectException;

import java.util.Collection;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandArg() {
        return "-h";
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Display list of terminal commands.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "help";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (serviceLocator == null) throw new NullObjectException();
        System.out.println("[HELP]");
        @NotNull final ICommandService commandService = serviceLocator.getCommandService();
        @NotNull final Collection<AbstractCommand> commands = commandService.getCommands();
        for (@NotNull final AbstractCommand command : commands) System.out.println(command);
    }

}
