package ru.ekfedorov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.repository.ITaskRepository;
import ru.ekfedorov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public final class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public Optional<Task> bindTaskByProjectId(
            @NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId
    ) {
        final Optional<Task> task = findOneById(userId, taskId);
        if (!task.isPresent()) return Optional.empty();
        task.get().setProjectId(projectId);
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @NotNull final String userId, @NotNull final String projectId
    ) {
        @NotNull final List<Task> list1 = new ArrayList<>();
        for (@NotNull final Task task : list) {
            if (projectId.equals(task.getProjectId()) &&
                    userId.equals(task.getUserId())) list1.add(task);
        }
        return list1;
    }

    @Override
    public void removeAllByProjectId(
            @NotNull final String userId, @NotNull final String projectId
    ) {
        list.removeIf(
                task -> projectId.equals(task.getProjectId()) &&
                        userId.equals(task.getUserId())
        );
    }

    @NotNull
    @Override
    public Optional<Task> unbindTaskFromProjectId(
            @NotNull final String userId, @NotNull final String taskId
    ) {
        @NotNull final Optional<Task> task = findOneById(userId, taskId);
        if (!task.isPresent()) return Optional.empty();
        task.get().setProjectId(null);
        return task;
    }

}
