package ru.ekfedorov.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.ekfedorov.tm.api.repository.ICommandRepository;
import ru.ekfedorov.tm.api.repository.IProjectRepository;
import ru.ekfedorov.tm.api.repository.ITaskRepository;
import ru.ekfedorov.tm.api.repository.IUserRepository;
import ru.ekfedorov.tm.api.service.*;
import ru.ekfedorov.tm.command.AbstractCommand;
import ru.ekfedorov.tm.command.system.ExitCommand;
import ru.ekfedorov.tm.component.Backup;
import ru.ekfedorov.tm.component.FileScanner;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.incorrect.IncorrectCommandException;
import ru.ekfedorov.tm.repository.CommandRepository;
import ru.ekfedorov.tm.repository.ProjectRepository;
import ru.ekfedorov.tm.repository.TaskRepository;
import ru.ekfedorov.tm.repository.UserRepository;
import ru.ekfedorov.tm.service.*;
import ru.ekfedorov.tm.util.SystemUtil;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

@Getter
public class Bootstrap implements ServiceLocator {

    @NotNull
    public final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    public final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    public final ILoggerService loggerService = new LoggerService();

    @NotNull
    public final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    public final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    public final IPropertyService propertyService = new PropertyService();

    @NotNull
    public final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    public final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    public final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    public final IUserRepository userRepository = new UserRepository();

    @NotNull
    public final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    public final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    private void displayWelcome() {
        loggerService.debug("***           TEST          ***");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
    }

    private void init() {
        initPID();
        initCommands();
        initUser();
        initDefaultData();
        initBackup();
        initFileScanner();
    }

    private void initBackup() {
        backup.init();
    }

    private void initFileScanner() {
        fileScanner.init();
    }

    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.ekfedorov.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.ekfedorov.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    private void initDefaultData() {
        final String userId = userRepository.findByLogin("test").get().getId();
        projectService.add(userId, "bbb", "-").setStatus(Status.IN_PROGRESS);
        projectService.add(userId, "bab", "-").setStatus(Status.COMPLETE);
        projectService.add(userId, "aaa", "-").setStatus(Status.NOT_STARTED);
        projectService.add(userId, "ccc", "-").setStatus(Status.COMPLETE);
        taskService.add(userId, "bbb", "-").setStatus(Status.IN_PROGRESS);
        taskService.add(userId, "aaa", "-").setStatus(Status.NOT_STARTED);
        taskService.add(userId, "ccc", "-").setStatus(Status.COMPLETE);
        taskService.add(userId, "eee", "-").setStatus(Status.NOT_STARTED);
        taskService.add(userId, "ddd", "-").setStatus(Status.IN_PROGRESS);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initUser() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    private void parseArg(@Nullable final String arg) {
        if (isEmpty(arg)) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        command.execute();
    }

    private boolean parseArgs(@Nullable final String... args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String param = args[0];
        parseArg(param);
        return true;
    }

    @SneakyThrows
    public void parseCommand(@Nullable final String cmd) {
        if (isEmpty(cmd)) return;
        @Nullable final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) throw new IncorrectCommandException(cmd);
        @Nullable final Role[] roles = command.commandRoles();
        authService.checkRoles(roles);
        command.execute();
    }

    private void process() {
        while (true) {
            try {
                System.out.println();
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                parseCommand(command);
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public void registry(@Nullable final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(final String... args) {
        displayWelcome();
        init();
        if (parseArgs(args)) new ExitCommand().execute();
        process();
    }

}
