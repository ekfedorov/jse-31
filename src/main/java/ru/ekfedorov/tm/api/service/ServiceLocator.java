package ru.ekfedorov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ServiceLocator {

    @NotNull
    IAuthService getAuthService();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

}
