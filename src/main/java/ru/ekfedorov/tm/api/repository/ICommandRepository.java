package ru.ekfedorov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(@NotNull AbstractCommand command);

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    Collection<String> getCommandArgs();

    @NotNull Collection<AbstractCommand> getArgsCommands();

    @Nullable
    AbstractCommand getCommandByArg(@NotNull String arg);

    @Nullable
    AbstractCommand getCommandByName(@NotNull String name);

    @NotNull
    Collection<String> getCommandName();

    @NotNull
    Collection<AbstractCommand> getCommands();

}
