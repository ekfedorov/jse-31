package ru.ekfedorov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IBusinessRepository<E extends AbstractBusinessEntity> extends IRepository<E> {

    void clear(@NotNull String userId);

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator);

    @NotNull
    List<E> findAll(@NotNull String userId);

    @NotNull
    Optional<E> findOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Optional<E> findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Optional<E> findOneByName(@NotNull String userId, @NotNull String name);

    boolean remove(@NotNull String userId, @NotNull E entity);

    boolean removeOneById(@NotNull String userId, @NotNull String id);

    boolean removeOneByIndex(
            @NotNull String userId, @NotNull Integer index
    );

    boolean removeOneByName(
            @NotNull String userId, @NotNull String name
    );

}
