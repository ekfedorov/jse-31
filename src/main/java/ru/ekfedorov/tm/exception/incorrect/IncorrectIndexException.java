package ru.ekfedorov.tm.exception.incorrect;

import ru.ekfedorov.tm.exception.AbstractException;

public final class IncorrectIndexException extends AbstractException {

    public IncorrectIndexException() throws Exception {
        super("Error! Index less then 0 OR is empty...");
    }

}
