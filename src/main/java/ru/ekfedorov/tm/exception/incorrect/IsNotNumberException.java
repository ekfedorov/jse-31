package ru.ekfedorov.tm.exception.incorrect;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.exception.AbstractException;

public final class IsNotNumberException extends AbstractException {

    public IsNotNumberException(@NotNull final String value) throws Exception {
        super("Error! " + value + " is not number...");
    }

}
